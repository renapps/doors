﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpriteSlicer2DDemoManager : MonoBehaviour 
{
	List<SpriteSlicer2DSliceInfo> m_SlicedSpriteInfo = new List<SpriteSlicer2DSliceInfo>();
	TrailRenderer m_TrailRenderer;

	struct MousePosition
	{
		public Vector3 m_WorldPosition;
		public float m_Time;
	}

	List<MousePosition> m_MousePositions = new List<MousePosition>();
	float m_MouseRecordTimer = 0.0f;
	float m_MouseRecordInterval = 0.05f;
	int m_MaxMousePositions = 5;
    bool m_FadeFragments = false;
	public Camera UIROOT;

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start ()
	{

		m_TrailRenderer = GetComponentInChildren<TrailRenderer>();
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update () 
	{


		if (Input.GetMouseButton (0))

		       {
				
				Vector3 mouseWorldPosition = UIROOT.ScreenToWorldPoint(Input.mousePosition);
				mouseWorldPosition.z = Camera.main.transform.position.z;
			    RaycastHit2D rayCastResult = Physics2D.Raycast(mouseWorldPosition, new Vector3(0, 0, 0), 0.0f);


//			Здесь по сути выпускается рэйкаст и создается переменная рэйкастРезалт,
//			в которой указаны его параметры по координатам (вроде бы). Нужно кроме этих координат 
//			получить и само название геймОбджекта, которое будет использоваться чуть ниже


			if (rayCastResult.rigidbody) 
			{
				if(Input.GetMouseButton(0)) // Вот тут надо добавить оператор && и в нём проверить булевую переменную в скрипте, в том геймобджекте,
					                        // полученном из результата выше
				{
					SpriteSlicer2D.ExplodeSprite(rayCastResult.rigidbody.gameObject, 16, 3f, true, ref m_SlicedSpriteInfo);

					if(m_SlicedSpriteInfo.Count == 0)
					{
						// Couldn't cut for whatever reason, add some force anyway
						rayCastResult.rigidbody.AddForce(new Vector2(0.0f, 400.0f));
					}
				}
			}
		}

		// Left mouse button - hold and swipe to cut objects
	


		// Sliced sprites sharing the same layer as standard Unity sprites could increase the draw call count as
		// the engine will have to keep swapping between rendering SlicedSprites and Unity Sprites.To avoid this, 
		// move the newly sliced sprites either forward or back along the z-axis after they are created
		for(int spriteIndex = 0; spriteIndex < m_SlicedSpriteInfo.Count; spriteIndex++)
		{
			for(int childSprite = 0; childSprite < m_SlicedSpriteInfo[spriteIndex].ChildObjects.Count; childSprite++)
			{
				Vector3 spritePosition = m_SlicedSpriteInfo[spriteIndex].ChildObjects[childSprite].transform.position;
				spritePosition.z = -1.0f;
				m_SlicedSpriteInfo[spriteIndex].ChildObjects[childSprite].transform.position = spritePosition;
			}
		}

        if(m_FadeFragments)
        {
            // If we've chosen to fade out fragments once an object is destroyed, add a fade and destroy component
            for (int spriteIndex = 0; spriteIndex < m_SlicedSpriteInfo.Count; spriteIndex++)
            {
                for (int childSprite = 0; childSprite < m_SlicedSpriteInfo[spriteIndex].ChildObjects.Count; childSprite++)
                {
                    if (!m_SlicedSpriteInfo[spriteIndex].ChildObjects[childSprite].GetComponent<Rigidbody2D>().isKinematic)
                    {
                        m_SlicedSpriteInfo[spriteIndex].ChildObjects[childSprite].AddComponent<FadeAndDestroy>();
                    }                    
                }
            }
        }

		m_SlicedSpriteInfo.Clear();
	}



	/// <summary>
	/// Draws the GUI
	/// </summary>
	void OnGUI () 
	{
		if(GUI.Button(new Rect(20,20,120,20), "Reset Scene")) 
		{
			Application.LoadLevel(Application.loadedLevel);
		}

        m_FadeFragments = GUI.Toggle(new Rect(20, 50, 400, 20), m_FadeFragments, "Fade out sprites once destroyed");

		GUI.Label(new Rect((Screen.width/2),20,900,20), "Left Mouse Button + Drag Cursor: Slice Objects");
		GUI.Label(new Rect((Screen.width/2),40,900,20), "(Cuts objects intersected by the cursor movement vector)");

		GUI.Label(new Rect((Screen.width/2),80,900,20), "Ctrl + Click Left Mouse Button: Explode Objects");
		GUI.Label(new Rect((Screen.width/2),100,900,20), "(Randomly slices an objects multiple times, then applies an optional force)");
	}

}

