Atlas3D Toolkit for NGUI
ver. 1.7

Atlas3D gives you only 1 drawcall 
with textured & lightmapped Meshes in Unity

You can:
- TEXTURE your imported 3Dmeshes with NGUI* Sprite Atlas
- modify PIVOT point
- change SCALE
- add color TINT
- REPAIR Unity lightmapping 
and keep 1 DRAWCALL !!!

All features are scriptable on dynamic objects without static batching.

Limitations:
- Unity will do dynamic batching on meshes that have not more than 300 vertices, scale of 1 and the same material.
- Skinned meshes are not compatible with Atlas3D because they are not batching at all even with the same material as other objects.
- Tilling is not supported. I am working on a solution in the next beta but it is not production ready.

Components were tested on NGUI 3.0.8, Unity 3.5.7 & 4.3, PC, Mac, iOS (iphone4s&ipad2), Android (GalaxyS2)
There is legacy version for NGUI 2.6.3 with basic features.

*You need NGUI to use this plugin

--------------------------------------------------------------------------------------------------------------

With Atlas3D you can use NGUI ATLAS to texture imported meshes using the same workflow for both UI and 3D content.
Unity will do dynamic batching on meshes that have not more than 300 vertices, scale of 1 and the same material.
You can add LIGHTMAPPING with Unity Editor but normally it will break dynamic batching.
With Atlas3D you can REPAIR lightmapping and still have 1 drawcall on dynamic objects.

It simply converts original Unity atlas information and modifies uvs on source object. 
This way you can still use built in lightmapping engine and keep 1 drawcall without static batching on Unity. 
This method has its drawbacks. Every lightmapped object needs to be unique. So you trade mesh sharing to get low on drawcalls. 

Atlas3D Toolkit is good for low-poly dynamic meshes in MOBILE games where you need low number of drawcalls.
It is useful for building world elements and putting lightmapping on them because they will batch even on indie version of Unity.
When you use unlit or vertex-lit shader that needs only diffuse texture atlas texturing gives you 1 drawcall even with big number of dynamic meshes.


Check the illustrated tutorial and video for basic functions:
http://tracki.pl/atlas3d/

http://youtu.be/rUqTlEOyv14

--------------------------------------------------------------------------------------------------------------

Support:

1. Support Forum (Ask questions here):
http://tracki.pl/forum

2. Unity3D forum thread:
http://forum.unity3d.com/threads/194216-Atlas3D-Toolkit-for-NGUI-Released

--------------------------------------------------------------------------------------------------------------

Main workflow:

1. Model 3D Objects
2. Import Objects into Unity (import settings)
3. Create Atlas using NGUI Atlas Maker
4. Apply NGUI Atlas on 3Dmeshes
5. Add LightMapping
6. Repair Lightmapping on 3Dmeshes
7. Change "PIVOT"
8. Change "SCALE"
9. Add Color "TINT"
10. Additional things you should know:


---

Main workflow:
1. Model 3D Objects
- Remember that dynamic batching in Unity works only with limited number of vertices (300 when you use simple shader and even less when using more complicated rendering)
- Make scale of the mesh to be 1
- Apply uv mapping for diffuse texture
- Unity can generate uvs for lighmapping so you can skip this in your 3D software
- Use single bitmaps to texture meshes (we will make atlas in unity)

2. Import Objects into Unity (import settings)
- Check "Generate Lightmap UV" (you need this only for lightmapping, if you will only use texturing you can skip this option)
- Select "import materials" and choose "By base texture name" script will use that to automatically select sprites from atlas
- Turn off "Rig" and "Animations" because we cant use skinned meshes with this componnent
- Put imported objects on scene add some light and check drawcalls (default "diffuse" shader will give you 2 x mesh count of drawcalls)

3. Create Atlas using NGUI Atlas Maker
- Importer will create materials in subfolder called "Materials", use the materials to create new Atlas or update existing Atlas
- You should get Atlas texture with combined bitmaps from your oryginal meshes. You can add bitmaps manually to the ATLAS.
- Set shader on Atlas material to "mobile/unlit(with lightmapping)" or something similar to get 1 drawcall in your mobile game

4. Apply NGUI Atlas on 3Dmeshes
- Select all meshes and put "MeshAtlas" Component on them (find it in Components/NGUI/Atlas3D)
- Now select Atlas in the component just like in regular NGUI workflow
- If you imported meshes correctly with materials (described in step 2) script will automatically select right sprite from Atlas and your meshes are ready
- You can always select Sprite manually
- Thats all with texturing - check your drawcalls - if you are lucky you have 1 drawcall for all the meshes 
- But hey we need some light&shadows on the scene...

5. Add LightMapping
- Select one of the meshes with Atlas3D component
- Change "Repair Lightmapping Mode" to "BAKE"
- Unity will render lightmaps to 1 or more textures based on your setup. First lightmap cost zero drawcall. Every new texture will cost 1 additional drawcall so if you can fit on single texture you are good. Adjust "Resolution" parameter to scale the map.
- Hit Bake Scene and wait for the result. When ready you will see that our drawcalls again are up to the numer of meshes + number of additional lightmap textures

6. Repair Lightmapping on 3Dmeshes
- Change "Repair Lightmapping Mode" to "REPAIR" to repair lightmapping - this will do the trick
- Check the number of drawcalls it should be 1 ;)
- Remember that every time you want to bake scene again - you need to choose "BAKE" Mode first. Then you can bake and when finished turn "REPAIR" Mode to get 1 drawcall result

7. Change "PIVOT"
- Imported mesh in Unity comes with own pivot. It is especially important if you want to scale a mesh during animation.

8. Change "SCALE"
- Yes you can change scale of the imported mesh during playback and keep 1 drawcall.
- Just remember to set gameobject scale to 1 in import settings. After that you can change scale in Atlas3D options.
- In Editor you can use "proportional scaling" by turning on the checkbox.

9. Add Color "TINT"
- To use "TINT" you need to change "Mesh Color Mode" to "TINT".
- You can add RGBA color to your mesh but to see effect you need to choose different shader that use "vertex color" and special blending when you set "ALPHA" in your tint.
- There are example shaders included with in Atlas3D folder.
- the interface for tint is similar to NGUI you can copy & paste colors.

10. Additional things you should know:
- "Update ATLAS" - hit this button when you change or update Atlas. This will refresh uvs on ALL the meshes that use the same Atlas as selected GameObject - no mater which mesh you select script will search scene automatically.
- "Update MESH" - Updates all the meshes in the scene. Hit it when you change any of the original mesh in 3D app.


--------------------------------------------

Version history:
1.7 (2014.01.7)
- Fixes for compatibility with NGUI 3.0.8 version.
- New feature: modify mesh PIVOT point
- New feature: change mesh SCALE
- New feature: add mesh color TINT

1.3 (2013.09.30)
- Fixes for compatibility with NGUI 3.0 version.

1.2 (2013.08.22)
- This version is a major update. Before installation you need to remove old componnents and delete the old Atlas3D folder
- Warning! To clear all the unwanted effects its advised to reimport all original meshes in the Assets in case they were modified by the previous version.
- Components are merged into single MeshAtlas component with atlasing and lightmapping option.
- Changed the method of uvs modification. Now the original mesh is never modified. Dynamic copies are generated in runtime for all the uvs changes.
- Simplified use without "make Unique" function. There are no more errors when meshes were instantiated.
- You can make prefabs from scene objects with MeshAtlas componnet without a problem.
- You can turn off component to display the original mesh with original material.
- Multiple objects modification. You can select many meshes on the scene and change atlas, texture, lightmapping options on all of them.
- When you add new mesh and you turn on lightmapping on, all other objects with MeshAtlas component that have lightmapping repaired will be turned to "BAKING MODE". Their "static" property will be turned on to be ready for lightmapping baking. Changing mode on one of the object automaticly changes this mode for other objects
- Added second example with the theme of poker cards.

1.1 (2013.08.09)
- You can put multiple original objects from the scene and there is no strange texture scaling after you put MeshTextureSelect component on them.

1.0 (2013.07.30)
- Original release of the toolkit.




