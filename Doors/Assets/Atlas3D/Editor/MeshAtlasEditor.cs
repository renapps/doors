using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CanEditMultipleObjects, CustomEditor(typeof(MeshAtlas))]
public class MeshAtlasEditor : Editor
{
	// ver. 1.7.0
	private SerializedObject t;
	protected MeshAtlas ma;
	
	private SerializedProperty
		pivot,
		mScale,
		scaleLink,
		colorTintState,
		repairLightmapState;
	
	private int tLength;
	public void OnEnable()
	{	
		ma = target as MeshAtlas;
		
		t = new SerializedObject(targets);
		tLength = targets.Length;
		
		pivot = t.FindProperty("pivot");
		mScale = t.FindProperty("mScale");
		scaleLink = t.FindProperty("scaleLink");
		colorTintState = t.FindProperty("colorTintState");
		repairLightmapState = t.FindProperty("repairLightmapState");
	}
	
	private static string[] toolbarBakeStrings = new string[] {"OFF", "BAKE"};
	private static string[] toolbarRepairStrings = new string[] {"OFF", "BAKE", "REPAIR"};
	private static string[] colorTintStrings = new string[] {"OFF", "TINT"};
	
	public Vector3 newScale;
	public override void OnInspectorGUI ()
	{
		t.Update();
		
		// Pivot
		GUILayout.BeginHorizontal();
			EditorGUILayout.PropertyField(pivot);
		GUILayout.EndHorizontal();
		
		// Scale
		Vector3 oldScale = mScale.vector3Value;
		GUILayout.BeginHorizontal();
			EditorGUILayout.PropertyField(mScale, new GUIContent("Scale"));
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
			NGUIEditorTools.DrawPaddedProperty("Proportional scaling", t, "scaleLink" );
			EditorGUILayout.LabelField("          ver 1.6");
		GUILayout.EndHorizontal();
		
		newScale = mScale.vector3Value;
		if (scaleLink.boolValue && oldScale!=newScale)
		{
			float sc;
			if (oldScale.x!=newScale.x)
			{
				sc = newScale.x/oldScale.x;
				newScale.y = newScale.y * sc;
				newScale.z = newScale.z * sc;
			}
			else if (oldScale.y!=newScale.y)
			{
				sc = newScale.y/oldScale.y;
				newScale.x = newScale.x * sc;
				newScale.z = newScale.z * sc;
			}
			else
			{
				sc = newScale.z/oldScale.z;
				newScale.x = newScale.x * sc;
				newScale.y = newScale.y * sc;
			}
			
			mScale.vector3Value = newScale;
			foreach(MeshAtlas s in targets)
			{
				s.scale = newScale;
			}
		}
		

		// atlas and sprite selection
		NGUIEditorTools.DrawSeparator();
		GUILayout.BeginHorizontal();
			if (NGUIEditorTools.DrawPrefixButton("Atlas"))
				ComponentSelector.Show<UIAtlas>(OnSelectAtlas);
			SerializedProperty atlas = NGUIEditorTools.DrawProperty("", t, "mAtlas");
			
			if (GUILayout.Button("Edit", GUILayout.Width(40f)))
			{
				if (atlas != null)
				{
					NGUIEditorTools.Select((atlas.objectReferenceValue as UIAtlas).gameObject);
				}
			}
		GUILayout.EndHorizontal();

		SerializedProperty sp = t.FindProperty("mSpriteName");
		NGUIEditorTools.DrawAdvancedSpriteField(atlas.objectReferenceValue as UIAtlas, sp.stringValue, SelectSprite, false);
		
		
		
		// update buttons
		GUILayout.BeginHorizontal();
			if (GUILayout.Button(new GUIContent("Update MESH",
				"Hit this button when you update original MESH. This will refresh ALL components that use the same mesh as selected GameObject")
				)) ma.UpdateAllMeshes();
			if (GUILayout.Button(new GUIContent("Update ATLAS",
				"Hit this button when you update Atlas. This will refresh uvs on ALL the meshes that use the same Atlas as selected GameObject")
				)) ma.UpdateMeshTextures();
		GUILayout.EndHorizontal();

		

		// Color tint
		NGUIEditorTools.DrawSeparator();
		EditorGUILayout.LabelField("Mesh Color MODE:");
		
		colorTintState.intValue = GUILayout.Toolbar(colorTintState.intValue, colorTintStrings);
		switch (colorTintState.intValue) 
		{
			case 1:
				GUILayout.BeginHorizontal();
				Color lastColor = ma.color;
				ma.color = EditorGUILayout.ColorField("Add Color Tint", ma.color);
				if (lastColor != ma.color) 
				{
					ma.UpdateMesh();
					foreach(MeshAtlas s in targets)
					{
						s.color = ma.color;
						s.UpdateMesh();
					}
				}
				if (GUILayout.Button("Copy", GUILayout.Width(50f)))
					NGUISettings.color = ma.color; //color.colorValue;
				GUILayout.EndHorizontal();
				
				GUILayout.BeginHorizontal();
				NGUISettings.color = EditorGUILayout.ColorField("Clipboard", NGUISettings.color);
				if (GUILayout.Button("Paste", GUILayout.Width(50f)))
				{
					ma.color = NGUISettings.color;
					ma.UpdateMesh();
					foreach(MeshAtlas s in targets)
					{
						s.color = NGUISettings.color;
						s.UpdateMesh();
					}
				}
				GUILayout.EndHorizontal();
			break;
			
			case 2:
				EditorGUILayout.LabelField("Not implemented...");
			break;
		}
		


		// lightmapping
		NGUIEditorTools.DrawSeparator();
		EditorGUILayout.LabelField("Repair Lightmapping Mode:");
		int newRepairLightmapState;
		if (ma.GetComponent<Renderer>().lightmapIndex<0) newRepairLightmapState = GUILayout.Toolbar(repairLightmapState.intValue, toolbarBakeStrings);
		else newRepairLightmapState = GUILayout.Toolbar(repairLightmapState.intValue, toolbarRepairStrings);

		switch (newRepairLightmapState) 
		{
			case 0:
				if (newRepairLightmapState!=repairLightmapState.intValue)
				{
					if (repairLightmapState.intValue == 2) ma.BakeMode();
					ma.repairLightmapState = 0;
					ma.gameObject.isStatic=false;
				}
			break;
			
			case 1:
				EditorGUILayout.HelpBox("BAKE MODE is ON. Now You can bake lightmapping. When you add lightmapping additional REPAIR option will show up and you will cut the number of drawcalls;)", MessageType.Info, true);
				if (newRepairLightmapState!=repairLightmapState.intValue) ma.BakeMode();
			break;
			
			case 2:
				EditorGUILayout.HelpBox("REPAIR MODE is ON to minimize drawcalls.\nRemember "+
				"to switch to BAKE MODE before you will render lightmapping again ;)", MessageType.Info, true);
				if (newRepairLightmapState!=repairLightmapState.intValue)
				{
					if (repairLightmapState.intValue == 0) ma.BakeMode();
					ma.RepairMode();
					t.Update();
				
				}
			break;
		}

		
		if(	t.ApplyModifiedProperties() 
			|| (Event.current.type == EventType.ValidateCommand 
			&& Event.current.commandName == "UndoRedoPerformed"))
		{
			foreach(MeshAtlas s in targets)
			{
				if (PrefabUtility.GetPrefabType(s) != PrefabType.Prefab) s.UpdateMesh();
			}
		}
	}
	

	
	
	public void OnDisable()
	{	
		if(target!= null && PrefabUtility.GetPrefabType(target) == PrefabType.Prefab)
		{
			((MeshAtlas)target).DisableMesh();
		}
	}
	

	// All widgets have a preview.
	public override bool HasPreviewGUI () 
	{ 
		return true;
	}


	/// Draw the sprite preview.
	public override void OnPreviewGUI (Rect rect, GUIStyle background)
	{
		if(tLength==1)
		{
			if (ma == null || !ma.isValid) return;
			if (ma.atlas == null) return;
			if (ma.atlas.spriteMaterial == null) return;
			
			Texture2D tex = ma.atlas.spriteMaterial.mainTexture as Texture2D;
			if (tex == null) return;
	
			UISpriteData sd = ma.atlas.GetSprite(ma.spriteName);
			NGUIEditorTools.DrawSprite(tex, rect, sd, Color.white);
		}
	}
	

	
	// Atlas selection callback.
	void OnSelectAtlas (Object obj)
	{
		foreach(MeshAtlas s in targets)
		{
			s.spriteName = null;
			s.atlas = obj as UIAtlas;
		}
	}

	// Sprite selection callback function.
	void SelectSprite (string spriteName)
	{
		foreach(MeshAtlas s in targets)
		{
			s.spriteName = spriteName;
			EditorUtility.SetDirty(s.gameObject);
		}
	}
		
}

