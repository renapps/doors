using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode, RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
[AddComponentMenu("NGUI/Atlas3D/MeshAtlas")]
public class MeshAtlas : MonoBehaviour
{
	// ver. 1.7.0
	public static UIAtlas lastUsedAtlas;
	
	// Mesh
	public Mesh originalMesh;
	private MeshFilter _mf;
	private Mesh mesh;
	
	// Material
	public Material originalMaterial;
	public Material customMaterial;
	
	// Atlas Sprite
	public UIAtlas mAtlas;
	public string mSpriteName;
	public UISpriteData mSprite;
	protected bool mSpriteSet = false;
	protected Rect mOuterUV = new Rect();
	
	// Repair Lightmapping
	public int repairLightmapState = 0;
	public Vector4 mlightmapScale;
	
	// Tint
	public bool colorTint;
	public int colorTintState = 0;
	public Color mColor = Color.white;
	
	// Pivot
	public Vector3 pivot = Vector3.zero;
	
	// Scale
	public Vector3 mScale = Vector3.one;
	public bool scaleLink = true;
	
	
	public void OnEnable()
	{
		UpdateMesh();
	}
	
	void OnDisable () 
	{
		DisableMesh();
	}
	
	void Reset () 
	{
		DisableMesh();
		if(Application.isEditor) UpdateMesh();
	}
	
	
	
	[SerializeField]
	public Vector3 scale 
	{ 
		get { return mScale; } 
		set { 
			//if (!mScale.Equals(value))
			//{
				mScale = value;
				CopyVertices(originalMesh,mesh);
			//}
		}
	}
	

	[SerializeField]
	public Color color 
	{ 
		get { return mColor; } 
		set { 
			//if (!mColor.Equals(value)) { mColor = value; } 
			mColor = value;
			if (colorTintState>0) UpdateTints();
		} 
	}

	
	public MeshFilter mf
	{
		get 
		{ 
			if (_mf==null) _mf = gameObject.GetComponent<MeshFilter>();
			return _mf; 
		}
	}
	
	
	
	public void UpdateMesh()
	{
		// store original mesh
		if (originalMesh==null) 
		{
			originalMesh = GetObjectMesh();
			originalMaterial = mf.GetComponent<Renderer>().sharedMaterial;
			if (atlas==null)
				if(lastUsedAtlas!=null) atlas = lastUsedAtlas;
		}
		
		if (enabled) EnableMesh();
		else DisableMesh();
	}
	
	
	
	// generate custom mesh based on original and modify uvs
	public void EnableMesh()
	{
		if (mesh==null)
		{
			mesh = new Mesh();
			mesh.name = originalMesh.name + "Atlas";
			mesh.hideFlags = HideFlags.HideAndDontSave;
		}
		
		CopyMesh (originalMesh, mesh);
		
		UpdateUVs();
		
		if (repairLightmapState==2) CalcLightmapUVs();
		
		if (customMaterial!=null) mf.GetComponent<Renderer>().sharedMaterial = customMaterial;
		mf.mesh = mesh;
	}
	
	
	
	// disable custom mesh and show original
	public void DisableMesh()
	{
		if (mesh!=null)
		{
			customMaterial = mf.GetComponent<Renderer>().sharedMaterial;
			DestroyImmediate(mesh);
			mesh = null;
		}
		
		if (originalMesh!=null) mf.mesh = originalMesh;
		if (originalMaterial!=null) mf.GetComponent<Renderer>().sharedMaterial = originalMaterial;
	}
	


	// Update texture UVs used by the mesh.
	virtual public void UpdateUVs (bool force=true)
	{
		if (atlas==null) return;
		if (string.IsNullOrEmpty(spriteName)) return;
		
		UISpriteData sp = atlas.GetSprite(spriteName);
		if (sp==null) return;
		if (atlas.texture==null) return;		
		mSprite = sp;
		
		// get sprite coords from Atlas
		mOuterUV.Set(mSprite.x, mSprite.y, mSprite.width, mSprite.height);
		Rect rect = NGUIMath.ConvertToTexCoords(mOuterUV, atlas.texture.width, atlas.texture.height);
	
		Vector2[] uvs = (Vector2[])originalMesh.uv.Clone();
		
		for (int i=0; i<uvs.Length; i++)
		{
			uvs[i].x = uvs[i].x * rect.width + rect.x;
			uvs[i].y = uvs[i].y * rect.height + rect.y;			
		}
		mesh.uv=uvs;
		
		// coloring
		if (colorTintState>0) UpdateTints();
	}
	
	
	public void UpdateTints ()
	{
		var muvlen = mesh.uv.Length;
		Color[] vcs = new Color[muvlen];

		for ( var i = 0; i < muvlen; i++ )
		{
			vcs[i] = color;
		}
		mesh.colors = vcs;
	}
		


	// NGUI Atlas
	[SerializeField]
	public UIAtlas atlas
	{
		get
		{
			return mAtlas;
		}
		set
		{
			if (mAtlas != value)
			{
				mAtlas = value;
				mSpriteSet = false;
				mSprite = null;
				if (mAtlas != null) 
				{
					lastUsedAtlas = value;
					customMaterial = mAtlas.spriteMaterial;
					// Automatically choose sprite based on oryginal texture name
					if (originalMaterial!=null && !string.IsNullOrEmpty(originalMaterial.name))
					{
						for (int i=0; i<mAtlas.spriteList.Count; i++)
						{
							if (mAtlas.spriteList[i].name == originalMaterial.name)
							{
								SetAtlasSprite(mAtlas.spriteList[i]);
								mSpriteName = mSprite.name;
								break;
							}
						}
					}
					
					// Automatically choose the first sprite
					if (string.IsNullOrEmpty(mSpriteName))
					{
						if (mAtlas != null && mAtlas.spriteList.Count > 0)
						{
							SetAtlasSprite(mAtlas.spriteList[0]);
							mSpriteName = mSprite.name;
						}
					}
	
					// Re-link the sprite
					if (!string.IsNullOrEmpty(mSpriteName))
					{
						string sprite = mSpriteName;
						mSpriteName = "";
						spriteName = sprite;
						#if UNITY_EDITOR
						//if (enabled) UpdateMesh();
						#endif
					}
					
					#if UNITY_EDITOR
					UnityEditor.EditorUtility.SetDirty(this);
					#endif
					//if (value == null) RevertOryginalMaterial();
				}
			}
		}
	}
	
	// Sprite within the atlas used to draw this widget.
	[SerializeField]
	public string spriteName
	{
		get
		{
			return mSpriteName;
		}
		set
		{
			if (string.IsNullOrEmpty(value))
			{
				// If the sprite name hasn't been set yet, no need to do anything
				if (string.IsNullOrEmpty(mSpriteName)) return;

				// Clear the sprite name and the sprite reference
				mSpriteName = "";
				mSprite = null;
				mSpriteSet = false;
			}
			else if (mSpriteName != value)
			{
				// If the sprite name changes, the sprite reference should also be updated
				mSpriteName = value;
				mSprite = null;
				mSpriteSet = false;
				#if UNITY_EDITOR
				UnityEditor.EditorUtility.SetDirty(this);
				if (isValid) UpdateMesh();
				#endif
				UpdateUVs();
			}
		}
	}


	public bool isValid { get { return GetAtlasSprite() != null; } }

	
	public UISpriteData GetAtlasSprite ()
	{
		if (!mSpriteSet) mSprite = null;

		if (mSprite == null && mAtlas != null)
		{
			if (!string.IsNullOrEmpty(mSpriteName))
			{
				UISpriteData sp = mAtlas.GetSprite(mSpriteName);
				if (sp == null) return null;
				SetAtlasSprite(sp);
			}

			if (mSprite == null && mAtlas.spriteList.Count > 0)
			{
				UISpriteData sp = mAtlas.spriteList[0];
				if (sp == null) return null;
				SetAtlasSprite(sp);

				if (mSprite == null)
				{
					Debug.LogError(mAtlas.name + " seems to have a null sprite!");
					return null;
				}
				mSpriteName = mSprite.name;
			}
		}
		return mSprite;
	}
	
	
	// Set the atlas sprite directly.
	protected void SetAtlasSprite (UISpriteData sp)
	{
		//mChanged = true;
		mSpriteSet = true;

		if (sp != null)
		{
			mSprite = sp;
			mSpriteName = mSprite.name;
		}
		else
		{
			mSpriteName = (mSprite != null) ? mSprite.name : "";
			mSprite = sp;
		}
	}
	

	
	
	// ----------------------------------------------
	private Mesh GetObjectMesh ()
	{
		MeshFilter meshf= gameObject.GetComponent<MeshFilter>();
		Mesh mesh;
		
		if (Application.isEditor){
			mesh=meshf.sharedMesh;
		} else {
			mesh=meshf.mesh;	
		}

		return mesh;
	}


	private int len;
	private Vector3[] vs;
	private void CopyMesh (Mesh meshSource, Mesh meshTarget)
	{
		CopyVertices(meshSource,meshTarget);
		meshTarget.triangles = 	(int[])meshSource.triangles.Clone();
		meshTarget.uv2 = 		(Vector2[])meshSource.uv2.Clone();
		meshTarget.normals = 	(Vector3[])meshSource.normals.Clone();
		meshTarget.colors = 	(Color[])meshSource.colors.Clone();
		meshTarget.tangents = 	(Vector4[])meshSource.tangents.Clone();
	}
	
	private void CopyVertices (Mesh meshSource, Mesh meshTarget)
	{
		if (meshSource && meshTarget)
		{
			vs = (Vector3[])meshSource.vertices.Clone();
			len = vs.Length;
			for (int i=0; i<len; i++)
			{
				vs[i].x = (vs[i].x + pivot.x) * scale.x;
				vs[i].y = (vs[i].y + pivot.y) * scale.y;
				vs[i].z = (vs[i].z + pivot.z) * scale.z;		
			}
			meshTarget.vertices = vs;
		}	
	}
	
	
	
	// find all meshes with the same atlas and update uvs
	public void UpdateAllMeshes ()
	{
		//List<MeshAtlas> list = NGUITools.FindAll<MeshAtlas>();
		var list = NGUITools.FindActive<MeshAtlas>();
		
		for (int i = 0, imax = list.Length; i < imax; ++i)
		{
			MeshAtlas sp = list[i];

			if (sp.enabled && originalMesh==sp.originalMesh)
			{
				sp.UpdateMesh();
				
				#if UNITY_EDITOR
				UnityEditor.EditorUtility.SetDirty(sp);
				#endif
			}
		}
	}
	
	
	
	// find all meshes with the same atlas and update uvs
	public void UpdateMeshTextures ()
	{
		#if UNITY_EDITOR
		UnityEditor.EditorUtility.SetDirty(gameObject);
		#endif
		
		var list = NGUITools.FindActive<MeshAtlas>();

		for (int i = 0, imax = list.Length; i < imax; ++i)
		{
			MeshAtlas sp = list[i];

			if (UIAtlas.CheckIfRelated(atlas, sp.atlas))
			{
				sp.UpdateMesh();
				
				#if UNITY_EDITOR
				UnityEditor.EditorUtility.SetDirty(sp);
				#endif
			}
		}
	}
	
	
	// =====================================================
	// LIGHTMAPPING
	// =====================================================
	
	// tiling/scaling for lightmapping
	[SerializeField]
	public Vector4 lightmapScale
	{
		get
		{
			if (mlightmapScale.magnitude==0) mlightmapScale=new Vector4(1,1,0,0);
			return mlightmapScale;
		}
		set
		{
			mlightmapScale = (Vector4)value;
		}
	}
	
	

	
	

	
	public void CalcLightmapUVs ()
	{
		var lm = lightmapScale;
	
		Vector2[] uv2s = (Vector2[])originalMesh.uv2.Clone();
		
		for (int i=0; i<uv2s.Length; i++)
		{
			uv2s[i].x = uv2s[i].x * lm.x + lm.z;
			uv2s[i].y = uv2s[i].y * lm.y + lm.w;			
		}
		
		#if UNITY_EDITOR
			UnityEditor.EditorUtility.SetDirty(this);
			//UnityEditor.EditorUtility.SetDirty(mesh);
			#endif
		
		mesh.uv2 = uv2s;
	}
	

	
	
	public void RepairMode ()
	{
		MeshAtlas[] list = NGUITools.FindActive<MeshAtlas>();
		for (int i = 0, imax = list.Length; i < imax; ++i)
		{
			MeshAtlas ma = list[i];
			
			if (ma.repairLightmapState==1)
			{
				ma.gameObject.isStatic = false;
				ma.repairLightmapState = 2;
				
				ma.lightmapScale = ma.GetComponent<Renderer>().lightmapScaleOffset;
				ma.GetComponent<Renderer>().lightmapScaleOffset = new Vector4(1,1,0,0);
				
				//ma.ConvertLightmapUVs();
				ma.UpdateMesh();
				
				#if UNITY_EDITOR
				UnityEditor.EditorUtility.SetDirty(ma);
				//UpdateMesh();
				#endif
			}
		}
	}
		
	public void BakeMode ()
	{
		MeshAtlas[] list = NGUITools.FindActive<MeshAtlas>();
		for (int i = 0, imax = list.Length; i < imax; ++i)
		{
			MeshAtlas ma = list[i];
			
			if (ma.repairLightmapState==2)
			{
				if (ma.lightmapScale.x <1 || ma.lightmapScale.y<1 || ma.lightmapScale.z>0 || ma.lightmapScale.w>0)
				{
					ma.GetComponent<Renderer>().lightmapScaleOffset = ma.lightmapScale;
				}
				ma.lightmapScale = new Vector4(1,1,0,0);
				ma.UpdateMesh();
			}
			
			ma.repairLightmapState = 1;
			ma.gameObject.isStatic = true;
			
			//Debug.Log("BakeMode");
			#if UNITY_EDITOR
			UnityEditor.EditorUtility.SetDirty(ma);
			//UpdateMesh();
			#endif
		}
	}
		
	
}

