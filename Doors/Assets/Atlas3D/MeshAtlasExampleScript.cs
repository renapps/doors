using UnityEngine;
using System.Collections;

public class MeshAtlasExampleScript : MonoBehaviour {

	public MeshAtlas 
		box,
		floor,
		stairs,
		man,
		cross;
		
	private float 
		a = 1f,
		r = 1f,
		g = 1f,
		b = 1f,
		s = 1f;
		
	// Use this for initialization
	void Start () {
		//box1 = transform.GetComponent<MeshAtlas>();
	}
	
	
	void Update () 
	{
		transform.Rotate(0,.1f,0);
		// scale animation
		a = 1 + .2f*Mathf.Sin(Time.time*4f);
		box.scale = new Vector3(a ,a ,a);
		
		// tint animation
		r = 1 + Mathf.Sin(Time.time*4f);
		g = 1 + Mathf.Sin(Time.time*5f);
		b = 1 + Mathf.Sin(Time.time*6f);
		stairs.color = new Color(r,g,b);
		
		
		// sprite scripting
		s = Mathf.Sin(Time.time*4f);
		if (s>0) cross.spriteName = "orange_texture";
		else cross.spriteName = "red_texture";
	}

}
