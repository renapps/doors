﻿using UnityEngine;
using System.Collections;

public class FPSFireManager : MonoBehaviour
{
    public ImpactInfo[] ImpactElemets = new ImpactInfo[0];
    public float BulletDistance = 10000;
    public GameObject ImpactEffect;


    
    [System.Serializable]
    public class ImpactInfo
    {
        public MaterialType.MaterialTypeEnum MaterialType;
        public GameObject ImpactEffect;
    }

    GameObject GetImpactEffect(GameObject impactedGameObject)
    {
        var materialType = impactedGameObject.GetComponent<MaterialType>();
        if (materialType==null)
            return null;
        foreach (var impactInfo in ImpactElemets)
        {
            if (impactInfo.MaterialType==materialType.TypeOfMaterial)
                return impactInfo.ImpactEffect;
        }
        return null;
    }


	public float rayDistance = 100;
	public Camera camera;
	void Update () {
		if (Input.GetMouseButtonDown(0)) {
			RaycastHit hit;
			Ray ray = camera.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast(ray, out hit, BulletDistance)) {
				
var effect = GetImpactEffect(hit.transform.gameObject);
				if (effect==null)
					return;
				var effectIstance = Instantiate(effect, hit.point, new Quaternion());
				ImpactEffect.SetActive(false);
				ImpactEffect.SetActive(true);
				effectIstance.transform.LookAt(hit.point + hit.normal);
				Destroy(effectIstance, 4);
			}

		}
	}







}


